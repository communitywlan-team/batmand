Source: batmand
Section: net
Priority: optional
Maintainer: Debian CommunityWLAN Team <team+communitywlan@tracker.debian.org>
Uploaders:
 Sven Eckelmann <sven@narfation.org>,
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://www.open-mesh.org/
Vcs-Git: https://salsa.debian.org/communitywlan-team/batmand.git -b debian/unstable
Vcs-Browser: https://salsa.debian.org/communitywlan-team/batmand
Build-Depends:
 debhelper-compat (= 13),

Package: batmand
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: better approach to mobile adhoc networking
 B.A.T.M.A.N. (better approach to mobile ad-hoc networking) is a new routing
 protocol for multi-hop ad-hoc mesh networks.
 .
 The approach of the B.A.T.M.A.N algorithm is to divide the knowledge about the
 best end-to-end paths between nodes in the mesh to all participating nodes.
 Each node perceives and maintains only the information about the best next hop
 towards all other nodes. Thereby the need for a global knowledge about local
 topology changes becomes unnecessary. Additionally, an event-based but timeless
 flooding mechanism prevents the accruement of contradicting topology
 information and limits the amount of topology messages flooding the mesh. The
 algorithm is designed to deal with networks that are based on unreliable links.
